﻿using System;
using InverseKinematics3D.Core;
using InverseKinematics3D.Utils.Math;
using OpenTK;

namespace InverseKinematics3D.Components
{
    public class Transform : Component, IDisposable
    {
        private Vector3 _position = Vector3.Zero;
        private Quaternion _rotationQ = Quaternion.Identity;
        private Vector3 _rotationE = Vector3.Zero;
        private Vector3 _scale = Vector3.One;

        public delegate void UpdateEventHandler();

        public event UpdateEventHandler OnPositionUpdate;
        public event UpdateEventHandler OnRotationUpdate;
        public event UpdateEventHandler OnScaleUpdate;

        public Vector3 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                 RaisePropertyChanged();
                OnPositionUpdate?.Invoke();
            }
        }

        public Quaternion Rotation
        {
            get { return _rotationQ; }
            set
            {
                _rotationQ = value;
                RaisePropertyChanged();
                OnRotationUpdate?.Invoke();
            }
        }

        public Vector3 RotationE
        {
            get { return _rotationE; }
            set
            {
                _rotationE = value;
                Rotation = Quaternion.FromEulerAngles(new Vector3(MathHelper.DegreesToRadians(_rotationE.Z), MathHelper.DegreesToRadians(_rotationE.Y), MathHelper.DegreesToRadians(_rotationE.X)));
                RaisePropertyChanged();
                OnRotationUpdate?.Invoke();
            }
        }

        public Vector3 Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                RaisePropertyChanged();
                OnScaleUpdate?.Invoke();
            }
        }

        private Matrix4 _rotationX = Matrix4.Identity;
        private Matrix4 _rotationY = Matrix4.Identity;
        private Matrix4 _rotationZ = Matrix4.Identity;
        
        public Matrix4 GetModelMatrix()
        {
            return MathGeneralUtils.CreateScale(Scale) * MathGeneralUtils.CreateFromQuaternion(Rotation.Normalized()) * MathGeneralUtils.CreateTranslation(Position);
        }

        public Transform()
        { }

        public Transform(UpdateEventHandler onPositionUpdate, UpdateEventHandler onRotationUpdate, UpdateEventHandler onScaleUpdate)
        {
            OnPositionUpdate += onPositionUpdate;
            OnRotationUpdate += onRotationUpdate;
            OnScaleUpdate += onScaleUpdate;
        }

        public void Dispose()
        {

        }
    }
}
