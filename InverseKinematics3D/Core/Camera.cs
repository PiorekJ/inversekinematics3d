﻿using InverseKinematics3D.Utils;
using InverseKinematics3D.Utils.Math;
using OpenTK;
using Key = System.Windows.Input.Key;
using MouseButton = System.Windows.Input.MouseButton;

namespace InverseKinematics3D.Core
{
    public class Camera : SceneObject
    {
        public float FOV { get; set; } = Constants.DefaultFOV;
        public float ZNear { get; set; } = Constants.DefaultFOV;
        public float ZFar { get; set; } = Constants.DefaultZFar;

        private Vector2 _lastMousePosition = Vector2.Zero;

        public float RotationX
        {
            get { return _rotationX; }
            set
            {
                _rotationX = value;
                RaisePropertyChanged();
            }
        }

        public float RotationY
        {
            get { return _rotationY; }
            set
            {
                _rotationY = value;
                RaisePropertyChanged();
            }
        }


        private float _aspectRatio;
        private float _rotationX;
        private float _rotationY;
        public bool IsMouseMoving => InputManager.IsMouseButtonDown(MouseButton.Middle);
        public bool IsKeyboardMoving => InputManager.IsKeyDown(Key.W) || InputManager.IsKeyDown(Key.S) || InputManager.IsKeyDown(Key.A) || InputManager.IsKeyDown(Key.D) || InputManager.IsKeyDown(Key.Space) || InputManager.IsKeyDown(Key.LeftCtrl);

        public bool IsRotating => InputManager.IsMouseButtonDown(MouseButton.Right);
        
        public Matrix4 GetViewMatrix()
        {
            return MathGeneralUtils.CreateTranslation(-Transform.Position) * MathGeneralUtils.CreateFromQuaternion(Transform.Rotation.Inverted());
        }

        public Matrix4 GetProjectionMatrix()
        {
            return MathGeneralUtils.CreatePerspectiveFOVMatrix(FOV, _aspectRatio, ZNear, ZFar);
        }

        public void SetViewportValues(float aspectRatio, int width, int height)
        {
            _aspectRatio = aspectRatio;
        }

        public Camera()
        {
            Transform.Position = Constants.DefaultCameraPosition;
            InputManager.OnMouseScrollEvent += Zoom;
        }

        public void UpdateCamera(Vector2 mousePosition)
        {
            if (!IsActive)
                return;
            MouseControl(mousePosition);
            //KeyboardControl();
        }

        private void MouseControl(Vector2 mousePosition)
        {
            if (_lastMousePosition == mousePosition)
                return;

            var diffX = mousePosition.X - _lastMousePosition.X;
            var diffY = mousePosition.Y - _lastMousePosition.Y;

            _lastMousePosition = mousePosition;

            if (IsMouseMoving)
            {
                Vector3 top = Vector3.Transform(-Vector3.UnitY, Transform.Rotation);
                Vector3 right = Vector3.Transform(Vector3.UnitX, Transform.Rotation);

                Transform.Position -= (top * diffY + right * diffX) * Constants.CameraMovementMouseSensitivity * Simulation.DeltaTime;
            }

            if (IsRotating)
            {
                RotationX -= diffY * Constants.CameraRotationMouseSensitivity * Simulation.DeltaTime;
                RotationY -= diffX * Constants.CameraRotationMouseSensitivity * Simulation.DeltaTime;

                Transform.Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                           Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
            }
        }

        private void Zoom(int delta)
        {
            if (delta < 0)
                Transform.Position += Transform.Rotation * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
            if (delta > 0)
                Transform.Position -= Transform.Rotation * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
        }

        private void KeyboardControl()
        {
            float velocity = InputManager.IsKeyDown(Key.LeftShift) ? Constants.CameraMovementKeySlowVelocity : Constants.CameraMovementKeyVelocity;

            if (InputManager.IsKeyDown(Key.W))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.S))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.A))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.D))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.Space))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.LeftCtrl))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }


        }

        public void SetLastMousePosition(Vector2 pos)
        {
            _lastMousePosition = pos;
        }

        protected override void OnRender() { }

        protected override void OnUpdate() { }
    }
}
