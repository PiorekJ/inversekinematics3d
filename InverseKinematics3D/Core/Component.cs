﻿using InverseKinematics3D.Utils.UIExtensionClasses;

namespace InverseKinematics3D.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }
    }
}
