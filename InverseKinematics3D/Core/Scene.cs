﻿using System.Collections.ObjectModel;
using System.Security.Policy;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics3D.Models;
using InverseKinematics3D.Utils.UIExtensionClasses;
using OpenTK;

namespace InverseKinematics3D.Core
{
    public class Scene : BindableObject
    {
        private SceneIPUMA _scenePumaI;
        private SceneQPUMA _scenePumaQ;
        private SceneCursor _startCursorI;
        private SceneCursor _startCursorQ;
        private SceneCursor _endCursorI;
        private SceneCursor _endCursorQ;
        public Camera Camera { get; set; }

        public SceneLight SceneLightI { get; set; }
        public SceneLight SceneLightQ { get; set; }


        public SceneBackground SceneBackgroundI { get; set; }
        public SceneBackground SceneBackgroundQ { get; set; }

        public ObservableCollection<Model> SceneObjectsI { get; set; }
        public ObservableCollection<Model> SceneObjectsQ { get; set; }

        public SceneCursor StartCursorI
        {
            get => _startCursorI;
            set
            {
                _startCursorI = value;
                RaisePropertyChanged();
            } 

        }

        public SceneCursor StartCursorQ
        {
            get => _startCursorQ;
            set
            {
                _startCursorQ = value;
                RaisePropertyChanged();
            } 
        }

        public SceneCursor EndCursorI
        {
            get => _endCursorI;
            set
            {
                _endCursorI = value;
                RaisePropertyChanged();
            } 
        }

        public SceneCursor EndCursorQ
        {
            get => _endCursorQ;
            set
            {
                _endCursorQ = value;
                RaisePropertyChanged();
            } 
        }

        public SceneDebugMesh SceneDebugMesh { get; set; }

        public SceneIPUMA ScenePumaI
        {
            get => _scenePumaI;
            set
            {
                _scenePumaI = value;
                RaisePropertyChanged();
            }
        }

        public SceneQPUMA ScenePumaQ
        {
            get => _scenePumaQ;
            set
            {
                _scenePumaQ = value;
                RaisePropertyChanged();
            }
        }

        public Scene()
        {
            Camera = new Camera();
            SceneObjectsI = new ObservableCollection<Model>();
            SceneObjectsQ = new ObservableCollection<Model>();
            InputManager.RegisterOnKeyDownEvent(Key.F1, () =>
            {
                StartCursorI.IsActive = true;
                StartCursorQ.IsActive = false;
                EndCursorI.IsActive = false;
                EndCursorQ.IsActive = false;
            });
            InputManager.RegisterOnKeyDownEvent(Key.F2, () =>
            {
                StartCursorI.IsActive = false;
                StartCursorQ.IsActive = false;
                EndCursorI.IsActive = true;
                EndCursorQ.IsActive = false;
            });

        }

        public void AddSceneCursorStartI()
        {
            StartCursorI = new SceneCursor(Colors.Red);
            StartCursorI.ObjectName = "XDD";
            StartCursorI.Transform.OnPositionUpdate += () =>
            {
                if (StartCursorQ != null)
                    StartCursorQ.Transform.Position = StartCursorI.Transform.Position;
            };

            StartCursorI.Transform.OnRotationUpdate += () =>
            {
                if (StartCursorQ != null)
                    StartCursorQ.Transform.Rotation = StartCursorI.Transform.Rotation;
            };
            StartCursorI.IsActive = false;
            StartCursorI.Transform.Position = new Vector3(2, 0.5f, 0);
        }

        public void AddSceneCursorStartQ()
        {
            StartCursorQ = new SceneCursor(Colors.Red);
            StartCursorQ.IsActive = false;
            StartCursorQ.Transform.Position = new Vector3(2, 0.5f, 0);
        }

        public void AddSceneCursorEndI()
        {
            EndCursorI = new SceneCursor(Colors.Blue);
            EndCursorI.Transform.OnPositionUpdate += () =>
            {
                if (EndCursorQ != null)
                    EndCursorQ.Transform.Position = EndCursorI.Transform.Position;
            };
            EndCursorI.Transform.OnRotationUpdate += () =>
            {
                if (EndCursorQ != null)
                    EndCursorQ.Transform.Rotation = EndCursorI.Transform.Rotation;
            };
            EndCursorI.IsActive = false;
        }

        public void AddSceneCursorEndQ()
        {
            EndCursorQ = new SceneCursor(Colors.Blue);
            EndCursorQ.IsActive = false;
        }

        public void AddSceneLightI()
        {
            SceneLightI = new SceneLight();
        }

        public void AddSceneLightQ()
        {
            SceneLightQ = new SceneLight();
        }

        public SceneBackground AddSceneBackgroundI()
        {
            SceneBackgroundI = new SceneBackground();
            return SceneBackgroundI;
        }

        public SceneBackground AddSceneBackgroundQ()
        {
            SceneBackgroundQ = new SceneBackground();
            return SceneBackgroundQ;
        }

        public ScenePUMA AddScenePumaI()
        {
            ScenePumaI = new SceneIPUMA();
            return ScenePumaI;
        }

        public ScenePUMA AddScenePumaQ()
        {
            ScenePumaQ = new SceneQPUMA();
            return ScenePumaQ;
        }
    }
}