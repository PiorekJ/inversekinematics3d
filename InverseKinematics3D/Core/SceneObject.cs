﻿using System;
using System.Collections.Generic;
using InverseKinematics3D.Components;
using InverseKinematics3D.Utils.UIExtensionClasses;

namespace InverseKinematics3D.Core
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }

    public class SceneObject : BindableObject, IDisposable
    {
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                RaisePropertyChanged();
            }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                RaisePropertyChanged();
            }
        }

        public bool IsDeletable = true;

        public Transform Transform { get; set; }

        private List<Component> _components;

        public delegate void DeleteHandler();

        public event DeleteHandler OnDeleteEvent;
        
        private bool _isVisible = true;
        private bool _isActive = true;

        public string ObjectName;

        public SceneObject()
        {
            _components = new List<Component>();
            Transform = AddComponent<Transform>();
        }

        public TComponent AddComponent<TComponent>()
            where TComponent : Component, new()
        {
            TComponent component = new TComponent { Owner = this };
            _components.Add(component);
            return component;
        }

        public TComponent GetComponent<TComponent>()
            where TComponent : Component
        {
            foreach (var item in _components)
            {
                if (item is TComponent)
                {
                    return (TComponent)item;
                }
            }
            return null;
        }

        public void RemoveComponent(Component component)
        {
            _components.Remove(component);
        }


        protected virtual void OnRender()
        {

        }

        protected virtual void OnUpdate()
        {

        }

        public void Render()
        {
            if (!IsVisible)
                return;

            OnRender();
        }

        public virtual void Update()
        {
            if (!IsActive)
                return;
            OnUpdate();
        }

        public void OnDelete()
        {
            OnDeleteEvent?.Invoke();
        }

        public virtual void Dispose()
        {
            RemoveComponent(Transform);
        }
    }
}
