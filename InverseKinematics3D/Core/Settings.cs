﻿using InverseKinematics3D.Utils.Math;
using InverseKinematics3D.Utils.UIExtensionClasses;
using OpenTK;

namespace InverseKinematics3D.Core
{
    public class Settings : BindableObject
    {
        private int _animationTime = 5;
        private bool _isRunning;
        private Vector3 _startPosition = Vector3.Zero;
        private Vector3 _endPosition = Vector3.Zero;
        private Vector3 _startRotationE = Vector3.Zero;
        private Vector3 _endRotationE = Vector3.Zero;
        private bool _uiEnabled = true;
        private float _l1 = 1.0f;
        private float _l3 = 1.0f;
        private float _l4 = 1.0f;

        public bool UIEnabled
        {
            get { return _uiEnabled; }
            set
            {
                _uiEnabled = value;
                RaisePropertyChanged();
            }
        }

        public int AnimationTime
        {
            get { return _animationTime; }
            set
            {
                _animationTime = value;
                RaisePropertyChanged();
            }
        }


        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 StartPosition
        {
            get { return _startPosition; }
            set
            {
                _startPosition = value;
                if (Simulation.Scene.StartCursorI != null)
                    Simulation.Scene.StartCursorI.Transform.Position = value;
                if (Simulation.Scene.StartCursorQ != null)
                    Simulation.Scene.StartCursorQ.Transform.Position = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 EndPosition
        {
            get { return _endPosition; }
            set
            {
                _endPosition = value;
                if (Simulation.Scene.EndCursorI != null)
                    Simulation.Scene.EndCursorI.Transform.Position = value;
                if (Simulation.Scene.EndCursorQ != null)
                    Simulation.Scene.EndCursorQ.Transform.Position = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 StartRotationE
        {
            get { return _startRotationE; }
            set
            {
                _startRotationE = value;

                var startRad = new Vector3(MathHelper.DegreesToRadians(_startRotationE.X), MathHelper.DegreesToRadians(_startRotationE.Y), MathHelper.DegreesToRadians(_startRotationE.Z));
                var startRotationQ = MathGeneralUtils.ToQuaternion(startRad.Z, startRad.Y, startRad.X);
                if (Simulation.Scene.StartCursorI != null)
                    Simulation.Scene.StartCursorI.Transform.Rotation = startRotationQ;
                if (Simulation.Scene.StartCursorQ != null)
                    Simulation.Scene.StartCursorQ.Transform.Rotation = startRotationQ;
                RaisePropertyChanged();
            }
        }

        public Vector3 EndRotationE
        {
            get { return _endRotationE; }
            set
            {
                _endRotationE = value;

                var endRad = new Vector3(MathHelper.DegreesToRadians(_endRotationE.X), MathHelper.DegreesToRadians(_endRotationE.Y), MathHelper.DegreesToRadians(_endRotationE.Z));
                var endRotationQ = MathGeneralUtils.ToQuaternion(endRad.Z, endRad.Y, endRad.X);
                if (Simulation.Scene.EndCursorI != null)
                    Simulation.Scene.EndCursorI.Transform.Rotation = endRotationQ;
                if (Simulation.Scene.EndCursorQ != null)
                    Simulation.Scene.EndCursorQ.Transform.Rotation = endRotationQ;
                RaisePropertyChanged();
            }
        }

        public float L1
        {
            get => _l1;
            set
            {
                _l1 = value;
                Simulation.Scene.ScenePumaQ.L1 = _l1;
                Simulation.Scene.ScenePumaI.L1 = _l1;
                RaisePropertyChanged();
            }
        }
        public float L3
        {
            get => _l3;
            set
            {
                _l3 = value;
                Simulation.Scene.ScenePumaQ.L3 = _l3;
                Simulation.Scene.ScenePumaI.L3 = _l3;
                RaisePropertyChanged();
            }
        }

        public float L4
        {
            get => _l4;
            set
            {
                _l4 = value;
                Simulation.Scene.ScenePumaQ.L4 = _l4;
                Simulation.Scene.ScenePumaI.L4 = _l4;
                RaisePropertyChanged();
            }
        }


    }
}
