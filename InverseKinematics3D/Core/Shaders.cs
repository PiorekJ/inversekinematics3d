﻿using InverseKinematics3D.OpenTK;

namespace InverseKinematics3D.Core
{
    public static class Shaders
    {
        private static Shader _basicShader;

        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _basicColorShader;

        public static Shader BasicColorShader
        {
            get
            {
                if (_basicColorShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicColor/BasicColorShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicColor/BasicColorShader.frag"))
                    {
                        _basicColorShader = new Shader(vert, frag);
                    }
                    return _basicColorShader;
                }
                return _basicColorShader;
            }
        }

        private static Shader _basicQuadShader;

        public static Shader BasicQuadShader
        {
            get
            {
                if (_basicQuadShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicQuad/BasicQuadShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicQuad/BasicQuadShader.frag"))
                    {
                        _basicQuadShader = new Shader(vert, frag);
                    }
                    return _basicQuadShader;
                }
                return _basicQuadShader;
            }
        }

        private static Shader _lightShader;
        public static Shader LightShader
        {
            get
            {
                if (_lightShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Light/LightShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Light/LightShader.frag"))
                    {
                        _lightShader = new Shader(vert, frag);
                    }
                    return _lightShader;
                }
                return _lightShader;
            }
        }

        private static Shader _litObjectShader;
        public static Shader LitObjectShader
        {
            get
            {
                if (_litObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Object/LightObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Object/LightObjectShader.frag"))
                    {
                        _litObjectShader = new Shader(vert, frag);
                    }
                    return _litObjectShader;
                }
                return _litObjectShader;
            }
        }

        private static Shader _litTexturedObjectShader;
        public static Shader LitTexturedObjectShader
        {
            get
            {
                if (_litTexturedObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/TexturedObject/LightTexturedObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/TexturedObject/LightTexturedObjectShader.frag"))
                    {
                        _litTexturedObjectShader = new Shader(vert, frag);
                    }
                    return _litTexturedObjectShader;
                }
                return _litTexturedObjectShader;
            }
        }
    }
}
