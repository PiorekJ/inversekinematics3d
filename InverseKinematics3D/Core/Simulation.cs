﻿using System.Windows.Input;
using InverseKinematics3D.Models;
using InverseKinematics3D.Utils;
using InverseKinematics3D.Utils.UIExtensionClasses;
using OpenTK;

namespace InverseKinematics3D.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static Settings Settings { get; set; }

        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        #region Commands

        public ICommand StartCommand { get; set; }
        public ICommand PauseCommand { get; set; }
        public ICommand StopCommand { get; set; }

        #endregion

        public Simulation()
        {
            Scene = new Scene();
            Settings = new Settings();
            TimeCounter = new TimeCounter();
            StartCommand = new RelayCommand(param => StartSimulation());
            PauseCommand = new RelayCommand(param => PauseSimulation());
            StopCommand = new RelayCommand(param => StopSimulation());
            TimeCounter.Start();
        }

        private void StartSimulation()
        {
            Scene.ScenePumaI.Reset();
            Scene.ScenePumaQ.Reset();
            Settings.UIEnabled = false;
            Settings.IsRunning = true;
            Scene.ScenePumaI.Initialize();
            Scene.ScenePumaQ.Initialize();
        }

        private void PauseSimulation()
        {
            Settings.IsRunning = false;
        }

        private void StopSimulation()
        {
            Settings.UIEnabled = true;
            Settings.IsRunning = false;
        }


        public void InitializeSimulationI()
        {
            Scene.AddSceneLightI();
            Scene.AddSceneBackgroundI();
            Scene.AddSceneCursorStartI();
            Scene.AddSceneCursorEndI();
            Scene.AddScenePumaI();
        }

        public void InitializeSimulationQ()
        {
            Scene.AddSceneLightQ();
            Scene.AddSceneBackgroundQ();
            Scene.AddSceneCursorStartQ();
            Scene.AddSceneCursorEndQ();
            Scene.AddScenePumaQ();
        }
    }
}
