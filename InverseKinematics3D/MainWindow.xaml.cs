﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics3D.Core;
using InverseKinematics3D.Utils.Math;
using OpenTK.Graphics.OpenGL;
using InputManager = InverseKinematics3D.Core.InputManager;

namespace InverseKinematics3D
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
        }

        private void DisplayControl_OnControlLeftLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.InitializeSimulationI();
            DisplayControlLeft.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlLeft.AspectRatio, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Viewport(0, 0, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);

            CompositionTarget.Rendering += OnRender;
        }

        private void DisplayControl_OnControlRightLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.InitializeSimulationQ();
            DisplayControlRight.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlRight.AspectRatio, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Viewport(0, 0, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
        }

        private void OnRender(object sender, EventArgs e)
        {
            Simulation.Scene.Camera.UpdateCamera(DisplayControlLeft.MousePosition);
            var dt = Simulation.TimeCounter.CountDT();

            

            DisplayControlRight.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlRight.AspectRatio, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Viewport(0, 0, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.ClearColor(System.Drawing.Color.Gray);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            DrawQuaternionScene();
            DisplayControlRight.SwapBuffers();

            DisplayControlLeft.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlLeft.AspectRatio, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Viewport(0, 0, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.ClearColor(System.Drawing.Color.Gray);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            DrawInverseScene();
            DisplayControlLeft.SwapBuffers();
        }

        private void DrawInverseScene()
        {
            Simulation.Scene.SceneBackgroundI.Render();
            Simulation.Scene.StartCursorI.Update();
            Simulation.Scene.StartCursorI.Render();
            Simulation.Scene.EndCursorI.Update();
            Simulation.Scene.EndCursorI.Render();
            Simulation.Scene.ScenePumaI.Update();
            Simulation.Scene.ScenePumaI.Render();
            foreach (Model model in Simulation.Scene.SceneObjectsI)
            {
                model.Render();
            }
        }

        private void DrawQuaternionScene()
        {
            Simulation.Scene.SceneBackgroundQ.Render();
            Simulation.Scene.StartCursorQ.Update();
            Simulation.Scene.StartCursorQ.Render();
            Simulation.Scene.EndCursorQ.Update();
            Simulation.Scene.EndCursorQ.Render();
            Simulation.Scene.ScenePumaQ.Update();
            Simulation.Scene.ScenePumaQ.Render();
            foreach (Model model in Simulation.Scene.SceneObjectsQ)
            {
                model.Render();
            }

        }

        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            DisplayControlLeft.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlLeft.AspectRatio, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Viewport(0, 0, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            DisplayControlRight.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlRight.AspectRatio, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Viewport(0, 0, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (Model item in Simulation.Scene.SceneObjectsI)
            {
                item.Dispose();
            }

            foreach (Model item in Simulation.Scene.SceneObjectsQ)
            {
                item.Dispose();
            }

            Simulation.Scene.SceneBackgroundI.Dispose();
            Simulation.Scene.SceneBackgroundQ.Dispose();
            Simulation.Scene.ScenePumaI.Dispose();
            Simulation.Scene.ScenePumaQ.Dispose();
            Simulation.Scene.StartCursorI.Dispose();
            Simulation.Scene.StartCursorQ.Dispose();
            Simulation.Scene.EndCursorI.Dispose();
            Simulation.Scene.EndCursorQ.Dispose();
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControlLeft.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }


    }
}
