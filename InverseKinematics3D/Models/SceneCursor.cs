﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics3D.Core;
using InverseKinematics3D.OpenTK;
using InverseKinematics3D.Utils;
using InverseKinematics3D.Utils.OpenTK;
using OpenTK;
using InputManager = InverseKinematics3D.Core.InputManager;

namespace InverseKinematics3D.Models
{
    public class SceneCursor : Model
    {
        private float _rotX = 0.0f;
        private float _rotY = 0.0f;
        private float _rotZ = 0.0f;

        public SceneCursor(Color color)
        {
            Transform.Position = Vector3.Zero;
            Shader = Shaders.LitObjectShader;
            Mesh = MeshGenerator.GenerateCursorCubeMesh(0.025f, 0.5f, 0.05f, color);
        }

        protected override void OnRender()
        {
            Shader.Use();

            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("lightColor"), Colors.White.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("lightPos"), Vector3.UnitY * 10);
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);

            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("ks"), 0.3f);
            Shader.Bind(Shader.GetUniformLocation("kd"), 0.75f);
            Shader.Bind(Shader.GetUniformLocation("ka"), 0.5f);
            Shader.Bind(Shader.GetUniformLocation("m"), 100.0f);
            Mesh.Draw();
        }

        private bool updateRot = false;

        protected override void OnUpdate()
        {
            var angle = Simulation.Scene.Camera.RotationY;
            var axis = Quaternion.FromAxisAngle(Vector3.UnitY, angle);
            if (InputManager.IsKeyDown(Key.W))
            {
                Transform.Position -=  axis * Vector3.UnitZ * Constants.CursorMovementKeySlowVelocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.S))
            {
                Transform.Position += axis * Vector3.UnitZ * Constants.CursorMovementKeySlowVelocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.A))
            {
                Transform.Position -= axis * Vector3.UnitX * Constants.CursorMovementKeySlowVelocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.D))
            {
                Transform.Position += axis * Vector3.UnitX * Constants.CursorMovementKeySlowVelocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.Space))
            {
                Transform.Position += axis * Vector3.UnitY * Constants.CursorMovementKeySlowVelocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.LeftCtrl))
            {
                Transform.Position -= axis * Vector3.UnitY * Constants.CursorMovementKeySlowVelocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.Y))
            {
                _rotX += Constants.CursorRotationKeySlowVelocity * Simulation.DeltaTime;
                updateRot = true;
            }
            if (InputManager.IsKeyDown(Key.H))
            {
                _rotX -= Constants.CursorRotationKeySlowVelocity * Simulation.DeltaTime;
                updateRot = true;
            }
            if (InputManager.IsKeyDown(Key.U))
            {
                _rotY += Constants.CursorRotationKeySlowVelocity * Simulation.DeltaTime;
                updateRot = true;
            }
            if (InputManager.IsKeyDown(Key.J))
            {
                _rotY -= Constants.CursorRotationKeySlowVelocity * Simulation.DeltaTime;
                updateRot = true;
            }
            if (InputManager.IsKeyDown(Key.I))
            {
                _rotZ += Constants.CursorRotationKeySlowVelocity * Simulation.DeltaTime;
                updateRot = true;
            }
            if (InputManager.IsKeyDown(Key.K))
            {
                _rotZ -= Constants.CursorRotationKeySlowVelocity * Simulation.DeltaTime;
                updateRot = true;
            }

            if (updateRot)
            {

                Transform.Rotation = Quaternion.FromEulerAngles(MathHelper.DegreesToRadians(_rotX),
                    MathHelper.DegreesToRadians(_rotY), MathHelper.DegreesToRadians(_rotZ));
                updateRot = false;
            }

        }

        private Mesh<VertexPC> GenerateCursorMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.UnitX * 0.5f, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.UnitY * 0.5f, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitZ));
            vertices.Add(new VertexPC(Vector3.UnitZ * 0.5f, Vector3.UnitZ));

            return new Mesh<VertexPC>(vertices, null, MeshType.Lines, AccessType.Static);
        }
    }
}
