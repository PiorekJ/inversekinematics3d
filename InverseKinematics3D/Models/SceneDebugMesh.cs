﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using InverseKinematics3D.Core;
using InverseKinematics3D.OpenTK;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace InverseKinematics3D.Models
{
    public class SceneDebugMesh : Model, IColorable
    {
        public Color Color { get; set; } = Colors.Red;

        public int GLLineWidth = 4;
        public int GLPointSize = 4;

        private List<VertexPC> _vertices;

        public SceneDebugMesh()
        {
            Shader = Shaders.BasicShader;
        }

        public void SetMesh(List<VertexPC> vertices, IEnumerable<uint> edges, MeshType type)
        {
            _vertices = vertices;
            Mesh = new Mesh<VertexPC>(vertices, edges, type, AccessType.Dynamic);
        }

        public void SetVertices(List<Vector3> positions)
        {
            for (int i = 0; i < _vertices.Count; i++)
            {
                _vertices[i] = new VertexPC(positions[i], _vertices[i].Color);
            }
            ((Mesh<VertexPC>)Mesh).SetVertices(0, _vertices.ToArray());
        }
        
        protected override void OnRender()
        {
            if (Mesh == null)
                return;

            GL.Disable(EnableCap.DepthTest);
            GL.LineWidth(GLLineWidth);
            GL.PointSize(GLPointSize);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.Draw();
            GL.LineWidth(1);
            GL.PointSize(1);
            GL.Enable(EnableCap.DepthTest);
        }

    }
}

