﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InverseKinematics3D.Core;
using InverseKinematics3D.Utils.Math;
using OpenTK;

namespace InverseKinematics3D.Models
{
    public class SceneIPUMA : ScenePUMA
    {
        private float _a0;
        private float _a1;
        private float _a2;
        private float _a3;
        private float _a4;
        private float _l2;

        private Vector3 _startP2;
        public override void CalculateStepAt(float t)
        {
            var posInter = Vector3.Lerp(Simulation.Scene.StartCursorI.Transform.Position, Simulation.Scene.EndCursorI.Transform.Position, t);
            var rotInter = Quaternion.Slerp(Simulation.Scene.StartCursorI.Transform.Rotation, Simulation.Scene.EndCursorI.Transform.Rotation, t);
            InverseKinematics(posInter, rotInter, _startP2, out _a0, out _a1, out _a2, out _a3, out _a4, out _l2);
            A0 = _a0;
            A1 = _a1;
            A2 = _a2;
            A3 = _a3;
            A4 = _a4;
            L2 = _l2;
        }

        public override void Initialize()
        {
            FindOptimalConfiguration(Simulation.Scene.StartCursorI.Transform.Position, Simulation.Scene.StartCursorI.Transform.Rotation, out _startP2, true);
        }

        public override void FinalStep()
        {
        }
    }
}
