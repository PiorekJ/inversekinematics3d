﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics3D.Core;
using InverseKinematics3D.OpenTK;
using InverseKinematics3D.Utils;
using InverseKinematics3D.Utils.Math;
using InverseKinematics3D.Utils.OpenTK;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using InputManager = InverseKinematics3D.Core.InputManager;

namespace InverseKinematics3D.Models
{
    public abstract class ScenePUMA : Model
    {
        public class ArmPart : IDisposable
        {
            public Color Color { get; set; }

            public Vector3 RotationAxis;
            public float RotationAngle;

            private Shader _shader;
            private IMesh _cyllinderMesh;
            private IMesh _jointMesh;

            private Matrix4 _cyllinderModelMatrix = Matrix4.Identity;
            private Matrix4 _jointModelMatrix = Matrix4.Identity;

            public ArmPart(Mesh<VertexPNC> cyllinderMesh, Mesh<VertexPNC> jointMesh)
            {
                _cyllinderMesh = cyllinderMesh;
                _jointMesh = jointMesh;
                _shader = Shaders.LitObjectShader;
            }

            public void SetModelMatrix(Matrix4 cyllinderMatrix, Matrix4 jointMatrix)
            {
                _cyllinderModelMatrix = cyllinderMatrix;
                _jointModelMatrix = jointMatrix;
            }

            public void OnRender()
            {
                _shader.Use();
                _shader.Bind(_shader.GetUniformLocation("model"), _cyllinderModelMatrix);
                _shader.Bind(_shader.GetUniformLocation("lightColor"), Constants.DefaultLightColor);
                _shader.Bind(_shader.GetUniformLocation("lightPos"), Constants.DefaultLightPosition);
                _shader.Bind(_shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                _shader.Bind(_shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                _shader.Bind(_shader.GetUniformLocation("alpha"), 1.0f);
                _shader.Bind(_shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
                _shader.Bind(_shader.GetUniformLocation("ks"), 0.3f);
                _shader.Bind(_shader.GetUniformLocation("kd"), 0.75f);
                _shader.Bind(_shader.GetUniformLocation("ka"), 0.5f);
                _shader.Bind(_shader.GetUniformLocation("m"), 100.0f);
                _cyllinderMesh?.Draw();
                _shader.Bind(_shader.GetUniformLocation("model"), _jointModelMatrix);
                _jointMesh.Draw();
            }

            public void Dispose()
            {
                _jointMesh?.Dispose();
                _cyllinderMesh?.Dispose();
            }
        }

        public ArmPart Arm0;
        public ArmPart Arm1;
        public ArmPart Arm2;
        public ArmPart Arm3;
        public ArmPart Arm4;
        private float _l1 = 1.0f;
        private float _l2 = 1.0f;
        private float _l3 = 1.0f;
        private float _l4 = 1.0f;

        private float _a0 = 0.0f;
        private float _a1 = 0.0f;
        private float _a2 = 0.0f;
        private float _a3 = 0.0f;
        private float _a4 = 0.0f;

        public float L1
        {
            get => _l1;
            set
            {
                _l1 = value;
                RaisePropertyChanged();
            }
        }

        public float L2
        {
            get => _l2;
            set
            {
                _l2 = value;
                RaisePropertyChanged();
            }
        }

        public float L3
        {
            get => _l3;
            set
            {
                _l3 = value;
                RaisePropertyChanged();
            }
        }

        public float L4
        {
            get => _l4;
            set
            {
                _l4 = value;
                RaisePropertyChanged();
            }

        }

        public float A0
        {
            get => _a0;
            set
            {
                _a0 = value;
                RaisePropertyChanged();
            }
        }

        public float A1
        {
            get => _a1;
            set
            {
                _a1 = value;
                RaisePropertyChanged();
            }
        }

        public float A2
        {
            get => _a2;
            set
            {
                _a2 = value;
                RaisePropertyChanged();
            }
        }

        public float A3
        {
            get => _a3;
            set
            {
                _a3 = value;
                RaisePropertyChanged();
            }
        }

        public float A4
        {
            get => _a4;
            set
            {
                _a4 = value;
                RaisePropertyChanged();
            }
        }

        public float CurrentTime { get; set; }

        private float JointR = 0.1f;
        private float JointH = 0.2f;

        public abstract void CalculateStepAt(float t);
        public abstract void Initialize();
        public abstract void FinalStep();

        public ScenePUMA()
        {
            var arm0Cyll = MeshGenerator.GenerateCylinderMesh(0.075f, 0, L1, 32, 32, Colors.DarkGray.ColorToVector3(),
                Matrix3.CreateScale(L1), Matrix4.Identity);
            var arm0Joint = MeshGenerator.GenerateCylinderMesh(JointR, 0, JointH, 32, 32, Colors.LawnGreen.ColorToVector3(), Matrix3.Identity,
                Matrix4.CreateTranslation(0, -JointH / 2.0f, 0));

            var arm1Cyll = MeshGenerator.GenerateCylinderMesh(0.075f, 0, L2, 32, 32, Colors.DarkGray.ColorToVector3(), Matrix3.CreateRotationZ(MathHelper.DegreesToRadians(-90)), Matrix4.Identity);
            var arm1Joint = MeshGenerator.GenerateCylinderMesh(JointR, 0, JointH, 32, 32, Colors.LawnGreen.ColorToVector3(), Matrix3.CreateRotationX(MathHelper.DegreesToRadians(90)), Matrix4.CreateTranslation(0, 0, -JointH / 2.0f));


            var arm2Cyll = MeshGenerator.GenerateCylinderMesh(0.075f, 0, L3, 32, 32, Colors.DarkGray.ColorToVector3(), Matrix3.CreateRotationZ(MathHelper.DegreesToRadians(-180)), Matrix4.Identity);
            var arm2Joint = MeshGenerator.GenerateCylinderMesh(JointR, 0, JointH, 32, 32, Colors.LawnGreen.ColorToVector3(), Matrix3.CreateRotationX(MathHelper.DegreesToRadians(90)), Matrix4.CreateTranslation(0, 0, -JointH / 2.0f));


            var arm3Cyll = MeshGenerator.GenerateCylinderMesh(0.075f, 0, L4, 32, 32, Colors.DarkGray.ColorToVector3(), Matrix3.CreateRotationZ(MathHelper.DegreesToRadians(-90)), Matrix4.Identity);
            var arm3Joint = MeshGenerator.GenerateCylinderMesh(JointR, 0, JointH, 32, 32, Colors.LawnGreen.ColorToVector3(), Matrix3.Identity, Matrix4.CreateTranslation(0, -JointH / 2.0f, 0));

            var arm4Joint = MeshGenerator.GenerateCursorCubeMesh(0.025f, 0.5f, 0, Colors.CornflowerBlue); //MeshGenerator.GenerateCylinderMesh(JointR, 0, JointH, 8, 8, Colors.CornflowerBlue.ColorToVector3(), Matrix3.CreateRotationZ(MathHelper.DegreesToRadians(-90)), Matrix4.CreateTranslation(-JointH / 2.0f, 0, 0));

            Arm0 = new ArmPart(arm0Cyll, arm0Joint);
            Arm1 = new ArmPart(arm1Cyll, arm1Joint);
            Arm2 = new ArmPart(arm2Cyll, arm2Joint);
            Arm3 = new ArmPart(arm3Cyll, arm3Joint);
            Arm4 = new ArmPart(null, arm4Joint);

            L3 = 0.5f;
        }

        private void AddDebugMesh(List<Vector3> positions, List<Vector3> colors, MeshType type)
        {
            Simulation.Scene.SceneDebugMesh = new SceneDebugMesh();
            var vertices = new List<VertexPC>();
            var edges = new List<uint>();
            for (var i = 0; i < positions.Count; i++)
            {
                var pos = positions[i];
                vertices.Add(new VertexPC(pos, colors[i]));
            }

            if (type == MeshType.Lines)
            {
                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    edges.Add((uint)i);
                    edges.Add((uint)i + 1);
                }
                Simulation.Scene.SceneDebugMesh.SetMesh(vertices, edges, type);
            }
            else
            {
                Simulation.Scene.SceneDebugMesh.SetMesh(vertices, null, type);
            }

            Simulation.Scene.SceneObjectsI.Add(Simulation.Scene.SceneDebugMesh);
        }

        private Vector3 _oldP1;
        private Vector3 _oldP2;
        private Vector3 _oldP3;
        private Vector3 _oldP4;

        private float _oldA0 = 0;
        private float _oldA1 = 0;
        private float _oldA2 = 0;
        private float _oldA3 = 0;
        private float _oldA4 = 0;

        protected void FindOptimalConfiguration(Vector3 effectorPosition, Quaternion effectorRotation, out Vector3 point2, bool overrideOldP2 = false)
        {
            var p = new Vector3[6];
            p[0] = Vector3.Zero;
            p[1] = new Vector3(0, L1, 0);
            p[3] = effectorPosition - (effectorRotation * Vector3.UnitX).Normalized() * L4;
            p[4] = effectorPosition;
            p[2] = Vector3.Zero;
            var normalToPlane = Vector3.Cross(p[3] - p[0], p[1] - p[0]).Normalized();
            var v1 = Vector3.Cross(p[4] - p[3], normalToPlane).Normalized();

            var p21 = p[3] - v1 * L3;
            var p22 = p[3] + v1 * L3;

            var l1 = (p21 - p[1]).Length;
            var l2 = (p22 - p[1]).Length;

            point2 = l1 <= l2 ? p21 : p22;

            if (overrideOldP2)
                _oldP2 = point2;
        }

        protected void InverseKinematics(Vector3 effectorPosition, Quaternion effectorRotation, Vector3 startP2Config, out float a0, out float a1, out float a2, out float a3, out float a4, out float l2)
        {

            var p = new Vector3[6];

            p[0] = Vector3.Zero;
            p[1] = new Vector3(0, L1, 0);
            p[3] = effectorPosition - (effectorRotation * Vector3.UnitX).Normalized() * L4;
            p[4] = effectorPosition;
            p[5] = effectorPosition + (effectorRotation * Vector3.UnitY).Normalized();
            p[2] = Vector3.Zero;

            var normalToPlane = Vector3.Cross(p[3] - p[0], p[1] - p[0]).Normalized();
            var v1 = Vector3.Cross(p[4] - p[3], normalToPlane).Normalized();
            var p21 = p[3] - v1 * L3;
            var p22 = p[3] + v1 * L3;

            var len1 = (p21 - _oldP2).Length;
            var len2 = (p22 - _oldP2).Length;
            p[2] = len1 <= len2 ? p21 : p22;
            //p[2] = p[3] - v1 * L3;


            //if ((p[2] - _oldP2).Length > 0.1 * L2)
            //    p[2] = p[3] + v1 * L3;

            l2 = (p[2] - p[1]).Length;

            _oldP2 = p[2];

            if (Simulation.Scene.SceneDebugMesh == null)
                AddDebugMesh(new List<Vector3>() { p[0], p[1], p[2], p[3], p[4] }, new List<Vector3>() { Colors.Black.ColorToVector3(), Colors.Red.ColorToVector3(), Colors.Blue.ColorToVector3(), Colors.Red.ColorToVector3(), Colors.Blue.ColorToVector3() }, MeshType.Lines);
            else
            {
                Simulation.Scene.SceneDebugMesh.SetVertices(new List<Vector3>() { p[0], p[1], p[2], p[3], p[4] });
            }

            a0 = MathHelper.RadiansToDegrees((float)Math.Atan2(p[3].Z, p[3].X));
            for (int i = 2; i < p.Length; i++)
            {
                p[i] = Matrix3.CreateRotationY(MathHelper.DegreesToRadians(-a0)) * (p[i] - p[1]);
            }

            a0 = -a0;

            a1 = MathHelper.RadiansToDegrees((float)Math.Atan2(p[2].Y, p[2].X));

            for (int i = 3; i < p.Length; i++)
            {
                p[i] = Matrix3.CreateRotationZ(MathHelper.DegreesToRadians(a1)) * (p[i] - p[2]);
            }

            a2 = MathHelper.RadiansToDegrees((float)Math.Atan2(p[3].Y, p[3].X)) + 90;

            for (int i = 4; i < p.Length; i++)
            {
                p[i] = Matrix3.CreateRotationZ(MathHelper.DegreesToRadians(a2)) * (p[i] - p[3]);
            }

            a3 = MathHelper.RadiansToDegrees((float)Math.Atan2(p[4].Z, p[4].X));

            p[5] = Matrix3.CreateRotationY(MathHelper.DegreesToRadians(-a3)) * (p[5] - p[4]);

            a3 = -a3;
            a4 = MathHelper.RadiansToDegrees((float)Math.Atan2(p[5].Z, p[5].Y));

            _oldA0 = a0;
            _oldA1 = a1;
            _oldA2 = a2;
            _oldA3 = a3;
            _oldA4 = a4;
        }

        protected override void OnUpdate()
        {
            if (InputManager.IsKeyDown(Key.NumPad0))
            {
                A0 += 100 * Simulation.DeltaTime;
                A0 %= 360;
            }

            if (InputManager.IsKeyDown(Key.NumPad1))
            {
                A1 += 100 * Simulation.DeltaTime;
                A1 %= 360;
            }

            if (InputManager.IsKeyDown(Key.NumPad2))
            {
                A2 += 100 * Simulation.DeltaTime;
                A2 %= 360;
            }

            if (InputManager.IsKeyDown(Key.NumPad3))
            {
                A3 += 100 * Simulation.DeltaTime;
                A3 %= 360;
            }

            if (InputManager.IsKeyDown(Key.NumPad4))
            {
                A4 += 100 * Simulation.DeltaTime;
                A4 %= 360;
            }

            if (InputManager.IsKeyDown(Key.NumPad5))
            {
                L1 += Simulation.DeltaTime;
            }
            if (InputManager.IsKeyDown(Key.NumPad6))
            {
                L2 += Simulation.DeltaTime;
            }
            if (InputManager.IsKeyDown(Key.NumPad7))
            {
                L3 += Simulation.DeltaTime;
            }
            if (InputManager.IsKeyDown(Key.NumPad8))
            {
                L4 += Simulation.DeltaTime;
            }

            var a0 = Matrix4.CreateRotationY(MathHelper.DegreesToRadians(A0));
            var a1 = Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(A1)) * Matrix4.CreateTranslation(0, L1, 0) * a0;
            var a2 = Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(A2)) * Matrix4.CreateTranslation(L2, 0, 0) * a1;
            var a3 = Matrix4.CreateRotationY(MathHelper.DegreesToRadians(A3)) * Matrix4.CreateTranslation(0, -L3, 0) * a2;
            var a4 = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(A4)) * Matrix4.CreateTranslation(L4, 0, 0) * a3;

            Arm0?.SetModelMatrix(Matrix4.CreateScale(1, L1, 1) * a0, a0);
            Arm1?.SetModelMatrix(Matrix4.CreateScale(L2, 1, 1) * a1, a1);
            Arm2?.SetModelMatrix(Matrix4.CreateScale(1, L3, 1) * a2, a2);
            Arm3?.SetModelMatrix(Matrix4.CreateScale(L4, 1, 1) * a3, a3);
            Arm4?.SetModelMatrix(Matrix4.Identity, a4);



            //InverseKinematics(a4.ExtractTranslation(), a4.ExtractRotation());
            //if (this is SceneIPUMA)
            //InverseKinematics(Simulation.Scene.StartCursorI.Transform.Position, Simulation.Scene.StartCursorI.Transform.Rotation, out _a0, out _a1, out _a2, out _a3, out _a4, out _l2);
            //else
            //{
            //    //Console.WriteLine($"a0: {A0} a1: {A1} a2: {A2} a3: {A3} a4: {A4}");
            if (Simulation.Settings.IsRunning)
            {
                if (CurrentTime < Simulation.Settings.AnimationTime)
                {
                    CalculateStepAt(CurrentTime / Simulation.Settings.AnimationTime);
                    CurrentTime += Simulation.DeltaTime;
                }
                else
                {
                    FinalStep();
                }
            }
            //}
        }

        public void Reset()
        {
            CurrentTime = 0.0f;
        }

        protected override void OnRender()
        {
            Arm0?.OnRender();
            Arm1?.OnRender();
            Arm2?.OnRender();
            Arm3?.OnRender();
            Arm4?.OnRender();
        }

        public override void Dispose()
        {
            Arm0?.Dispose();
            Arm1?.Dispose();
            Arm2?.Dispose();
            Arm3?.Dispose();
            Arm4?.Dispose();
        }
    }
}
