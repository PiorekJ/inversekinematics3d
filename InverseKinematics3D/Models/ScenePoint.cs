﻿using System.Collections.Generic;
using System.Windows.Media;
using InverseKinematics3D.Core;
using InverseKinematics3D.OpenTK;
using InverseKinematics3D.Utils;

namespace InverseKinematics3D.Models
{
    public class ScenePoint : Model, IColorable, ISelectable
    {
        private Color _color = Colors.Red;
        public Color Color
        {
            get => _color;
            set
            {
                _color = value;
                RaisePropertyChanged();
            }
        }

        public bool IsSelected { get; set; } = false;

        public ScenePoint()
        {
            Shader = Shaders.BasicColorShader;
            Mesh = GeneratePointMesh();
        }

        private IMesh GeneratePointMesh()
        {
            Mesh?.Dispose();
            return new Mesh<VertexP>(new List<VertexP>() { new VertexP(Transform.Position) }, null, MeshType.Points, AccessType.Static);
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());

            Mesh.Draw();
        }

      
    }
}
