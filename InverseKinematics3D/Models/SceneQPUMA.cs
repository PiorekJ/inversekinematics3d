﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InverseKinematics3D.Core;
using InverseKinematics3D.Utils.Math;
using OpenTK;

namespace InverseKinematics3D.Models
{
    public class SceneQPUMA : ScenePUMA
    {
        private Vector3 _storedStartPos;
        private Vector3 _storedEndPos;

        private Quaternion _storedStartRot;
        private Quaternion _storedEndRot;

        private float[] startConfig;
        private float[] endConfig;


       
        private float NormalizeAngle360(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle <= 0)
                angle += 360;

            return angle;
        }

        public override void Initialize()
        {
            _storedStartPos = Simulation.Scene.StartCursorQ.Transform.Position;
            _storedStartRot = Simulation.Scene.StartCursorQ.Transform.Rotation;
            _storedEndPos = Simulation.Scene.EndCursorQ.Transform.Position;
            _storedEndRot = Simulation.Scene.EndCursorQ.Transform.Rotation;
            startConfig = new float[6];
            endConfig = new float[6];
            var startP2 = Vector3.Zero;
            var endP2 = Vector3.Zero;
            FindOptimalConfiguration(_storedStartPos, _storedStartRot, out startP2, true);
            FindOptimalConfiguration(_storedEndPos, _storedEndRot, out endP2);

            InverseKinematics(_storedStartPos, _storedStartRot, startP2, out startConfig[0], out startConfig[1], out startConfig[2], out startConfig[3], out startConfig[4], out startConfig[5]);
            InverseKinematics(_storedEndPos, _storedEndRot, endP2, out endConfig[0], out endConfig[1], out endConfig[2], out endConfig[3], out endConfig[4], out endConfig[5]);

            startConfig[0] = NormalizeAngle360(startConfig[0]);
            startConfig[1] = NormalizeAngle360(startConfig[1]);
            startConfig[2] = NormalizeAngle360(startConfig[2]);
            startConfig[3] = NormalizeAngle360(startConfig[3]);
            startConfig[4] = NormalizeAngle360(startConfig[4]);

            endConfig[0] = NormalizeAngle360(endConfig[0]);
            endConfig[1] = NormalizeAngle360(endConfig[1]);
            endConfig[2] = NormalizeAngle360(endConfig[2]);
            endConfig[3] = NormalizeAngle360(endConfig[3]);
            endConfig[4] = NormalizeAngle360(endConfig[4]);

            if (startConfig[0] - endConfig[0] > 180)
                startConfig[0] -= 360;
            if (startConfig[0] - endConfig[0] <= -180)
                endConfig[0] -= 360;

            if (startConfig[1] - endConfig[1] > 180)
                startConfig[1] -= 360;
            if (startConfig[1] - endConfig[1] <= -180)
                endConfig[1] -= 360;

            if (startConfig[2] - endConfig[2] > 180)
                startConfig[2] -= 360;
            if (startConfig[2] - endConfig[2] <= -180)
                endConfig[2] -= 360;

            if (startConfig[3] - endConfig[3] > 180)
                startConfig[3] -= 360;
            if (startConfig[3] - endConfig[3] <= -180)
                endConfig[3] -= 360;

            if (startConfig[4] - endConfig[4] > 180)
                startConfig[4] -= 360;
            if (startConfig[4] - endConfig[4] <= -180)
                endConfig[4] -= 360;
        }

        public override void CalculateStepAt(float t)
        {
            //Initialize();
            A0 = MathGeneralUtils.Lerp(startConfig[0], endConfig[0], t);
            A1 = MathGeneralUtils.Lerp(startConfig[1], endConfig[1], t);
            A2 = MathGeneralUtils.Lerp(startConfig[2], endConfig[2], t);
            A3 = MathGeneralUtils.Lerp(startConfig[3], endConfig[3], t);
            A4 = MathGeneralUtils.Lerp(startConfig[4], endConfig[4], t);
            L2 = MathGeneralUtils.Lerp(startConfig[5], endConfig[5], t);
        }

        public override void FinalStep()
        {
            A0 = endConfig[0];
            A1 = endConfig[1];
            A2 = endConfig[2];
            A3 = endConfig[3];
            A4 = endConfig[4];
            L2 = endConfig[5];
        }
    }
}
