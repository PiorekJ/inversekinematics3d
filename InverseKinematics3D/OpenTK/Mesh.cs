﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;

namespace InverseKinematics3D.OpenTK
{
    public enum AccessType
    {
        Static,
        Dynamic,
        Stream
    }

    public enum MeshType
    {
        Points,
        Lines,
        Triangles,
        TriangleStrip
    }

    public interface IMesh : IDisposable
    {
        void Draw();
    }

    public class Mesh<TVertex> : IMesh
        where TVertex : struct, IVertex
    {
        private PrimitiveType _meshPrimitiveType;

        private int _vao;
        private int _vbo;
        private int _ebo;

        private TVertex[] _vertices;
        private uint[] _edges;

        public Mesh(IEnumerable<TVertex> vertices, IEnumerable<uint> edges, MeshType type, AccessType accessType)
        {
            BufferUsageHint usageHint = AccessTypeToBufferUsageHint(accessType);

            if (vertices == null)
                throw new NullReferenceException("Vertices not initialized");

            _vertices = vertices.ToArray();

            if (edges != null)
                _edges = edges.ToArray();
            else
            {
                _edges = new uint[_vertices.Length];
                for (uint i = 0; i < _edges.Length; i++)
                {
                    _edges[i] = i;
                }
            }

            _vao = GL.GenVertexArray();
            _vbo = GL.GenBuffer();
            _ebo = GL.GenBuffer();


            _meshPrimitiveType = MeshTypeToPrimitiveType(type);



            GL.BindVertexArray(_vao);

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(_vertices.Length * Marshal.SizeOf<TVertex>()), _vertices, usageHint);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(_edges.Length * sizeof(uint)), _edges, usageHint);

            TVertex vertex = new TVertex();
            vertex.Initialize();
            GL.BindVertexArray(0);
        }

        public void Draw()
        {
            GL.BindVertexArray(_vao);
            GL.DrawElements(_meshPrimitiveType, _edges.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        public void DrawTexture(int texId)
        {
            GL.BindTexture(TextureTarget.ProxyTexture2D, texId);
            GL.BindVertexArray(_vao);
            GL.DrawElements(_meshPrimitiveType, _edges.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        public void SetVertices(uint startIndex, TVertex[] vertices)
        {
            for (int i = 0; i < vertices.Length; ++i)
                _vertices[startIndex + i] = vertices[i];

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(startIndex * Marshal.SizeOf<TVertex>()), (IntPtr)(vertices.Length * Marshal.SizeOf<TVertex>()), vertices);
        }

        public void SetVertices(uint startIndex, uint endIndex, TVertex[] vertices)
        {
            var size = (endIndex - startIndex) + 1;
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(startIndex * Marshal.SizeOf<TVertex>()), (IntPtr)(size * Marshal.SizeOf<TVertex>()), ref vertices[startIndex]);
        }

        public void SetVertex(uint startIndex, ref TVertex vert)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(startIndex * Marshal.SizeOf<TVertex>()), (IntPtr)(Marshal.SizeOf<TVertex>()), ref vert);
        }

        #region IDisposable

        private bool disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        [HandleProcessCorruptedStateExceptions]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (disposed)
                    return;

                if (disposing)
                {
                }

                if (_ebo != 0)
                {
                    GL.DeleteBuffer(_ebo);
                    _ebo = 0;
                }

                if (_vbo != 0)
                {
                    GL.DeleteBuffer(_vbo);
                    _vbo = 0;
                }

                if (_vao != 0)
                {
                    GL.DeleteVertexArray(_vao);
                    _vao = 0;
                }

                disposed = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        ~Mesh()
        {
            Dispose(false);
        }

        #endregion


        private static BufferUsageHint AccessTypeToBufferUsageHint(AccessType type)
        {
            switch (type)
            {
                case AccessType.Dynamic:
                    return BufferUsageHint.DynamicDraw;
                case AccessType.Static:
                    return BufferUsageHint.StaticDraw;
                case AccessType.Stream:
                    return BufferUsageHint.StreamDraw;
                default:
                    throw new ArgumentOutOfRangeException("Access type not found");
            }
        }

        private static PrimitiveType MeshTypeToPrimitiveType(MeshType type)
        {
            switch (type)
            {
                case MeshType.Points:
                    return PrimitiveType.Points;
                case MeshType.Lines:
                    return PrimitiveType.Lines;
                case MeshType.Triangles:
                    return PrimitiveType.Triangles;
                case MeshType.TriangleStrip:
                    return PrimitiveType.TriangleStrip;
                default:
                    throw new ArgumentOutOfRangeException("Mesh type not found");
            }
        }
    }
}
