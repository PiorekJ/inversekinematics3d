﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace InverseKinematics3D.OpenTK
{
    public class Texture
    {
        public int TexID { get; private set; }

        public Texture(string filepath)
        {
            TexID = CreateFromFile(filepath);
        }


        private int CreateFromFile(string filepath)
        {
            Bitmap bitMap;
            try
            {
                bitMap = new Bitmap(filepath);
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Creating bitmap: " + e.Message);
            }

            BitmapData bitMapData = bitMap.LockBits(new Rectangle(0, 0, bitMap.Width, bitMap.Height),
                ImageLockMode.ReadOnly, bitMap.PixelFormat);
            var tex = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, bitMapData.Width, bitMapData.Height, 0, PixelFormat.Bgr, PixelType.UnsignedByte, bitMapData.Scan0);
            bitMap.UnlockBits(bitMapData);

            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            return tex;
        }
    }
}
