﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using OpenTK;
using OpenTK.Graphics;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace InverseKinematics3D.UserControls
{
    public partial class GL_DisplayControl : UserControl
    {
        private GLControl glControl;

        public static readonly RoutedEvent ControlLoadedEvent = EventManager.RegisterRoutedEvent(nameof(ControlLoaded),
            RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(GL_DisplayControl));

        public event RoutedEventHandler ControlLoaded
        {
            add { AddHandler(ControlLoadedEvent, value); }
            remove { RemoveHandler(ControlLoadedEvent, value); }
        }

        public static readonly RoutedEvent ControlUnloadedEvent = EventManager.RegisterRoutedEvent(nameof(ControlUnloaded),
            RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(GL_DisplayControl));

        public event RoutedEventHandler ControlUnloaded
        {
            add { AddHandler(ControlUnloadedEvent, value); }
            remove { RemoveHandler(ControlUnloadedEvent, value); }
        }

        public static readonly RoutedEvent ControlResizedEvent = EventManager.RegisterRoutedEvent(nameof(ControlResized),
            RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(GL_DisplayControl));

        public event RoutedEventHandler ControlResized
        {
            add { AddHandler(ControlResizedEvent, value); }
            remove { RemoveHandler(ControlResizedEvent, value); }
        }


        public GL_DisplayControl()
        {
            InitializeComponent();

            glControl = new GLControl(new GraphicsMode(ColorFormat.Empty, 24, 0), 3, 3, GraphicsContextFlags.Default);

            glControl.MouseDown += OnMouseDown;
            glControl.MouseUp += OnMouseUp;
            glControl.MouseWheel += OnMouseWheel;
            glControl.Load += GlControlLoad;
            glControl.Disposed += GlControlUnload;
            glControl.SizeChanged += GlControlSizeChanged;

            FormsHost.Child = glControl;
        }

        private void GlControlSizeChanged(object sender, EventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ControlResizedEvent, this));
        }

        private void GlControlUnload(object sender, EventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ControlUnloadedEvent, this));
        }

        private void GlControlLoad(object sender, EventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ControlLoadedEvent, this));
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.None)
                return;

            RaiseEvent(new MouseButtonEventArgs(Mouse.PrimaryDevice, 0, ConvertToWPFButton(e.Button))
            {
                RoutedEvent = MouseDownEvent,
                Source = this
            });

        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.None)
                return;

            RaiseEvent(new MouseButtonEventArgs(Mouse.PrimaryDevice, 0, ConvertToWPFButton(e.Button))
            {
                RoutedEvent = MouseUpEvent,
                Source = this
            });

        }

        private void OnMouseWheel(object sender, MouseEventArgs e)
        {
            RaiseEvent(new MouseWheelEventArgs(Mouse.PrimaryDevice, 0, e.Delta)
            {
                RoutedEvent = Mouse.MouseWheelEvent,
                Source = this
            });
        }

        private MouseButton ConvertToWPFButton(MouseButtons pressedButton)
        {
            switch (pressedButton)
            {
                case MouseButtons.Left:
                    return MouseButton.Left;
                case MouseButtons.Right:
                    return MouseButton.Right;
                case MouseButtons.Middle:
                    return MouseButton.Middle;
                case MouseButtons.XButton1:
                    return MouseButton.XButton1;
                case MouseButtons.XButton2:
                    return MouseButton.XButton2;
                default:
                    throw new Exception("Unknown mouse button pressed");
            }
        }

        public int ControlWidth
        {
            get { return glControl.Width; }
        }

        public int ControlHeight
        {
            get { return glControl.Height; }
        }

        public float AspectRatio
        {
            get { return glControl.AspectRatio; }
        }

        public void SwapBuffers()
        {
            glControl.SwapBuffers();
        }

        public void MakeCurrent()
        {
            glControl.MakeCurrent();
        }

        public Vector2 MousePosition
        {
            get
            {
                var pos = glControl.PointToClient(Control.MousePosition);
                return new Vector2(pos.X, pos.Y);
            }
        }
    }
}
