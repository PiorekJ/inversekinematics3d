﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OpenTK;

namespace InverseKinematics3D.UserControls
{
    /// <summary>
    /// Interaction logic for Vector3TextBox.xaml
    /// </summary>
    public partial class Vector4TextBox : UserControl, INotifyPropertyChanged
    {
        public Vector4 Value
        {
            get
            {
                return (Vector4)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public IValueConverter CustomConverter { get; set; }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof(Value), typeof(Vector4), typeof(Vector4TextBox), new PropertyMetadata(Vector4.Zero, OnValueChanged));

        public float X
        {
            get { return (float)Math.Round(((Vector4)GetValue(ValueProperty)).X, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector4(value, Value.Y, Value.Z, Value.W);
                OnPropertyChanged();
            }
        }

        public float Y
        {
            get { return (float)Math.Round(((Vector4)GetValue(ValueProperty)).Y, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector4(Value.X, value, Value.Z, Value.W);
                OnPropertyChanged();
            }
        }

        public float Z
        {
            get { return (float)Math.Round(((Vector4)GetValue(ValueProperty)).Z, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector4(Value.X, Value.Y, value, Value.W);
                OnPropertyChanged();
            }
        }

        public float W
        {
            get { return (float)Math.Round(((Vector4)GetValue(ValueProperty)).W, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector4(Value.X, Value.Y, Value.Z, value);
                OnPropertyChanged();
            }
        }


        public Vector4TextBox()
        {
            InitializeComponent();
            ((FrameworkElement)Content).DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector4TextBox control = sender as Vector4TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("X"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Y"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Z"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("W"));
            }
        }
    }
}
