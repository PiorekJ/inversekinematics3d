﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace InverseKinematics3D.Utils.Converters
{
    class FloatToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value.ToString().EndsWith("."))
                    return ".";
                if (value.ToString().EndsWith("-"))
                    return "-";

                return value;
            }

            return String.Empty;
        }
    }
}
