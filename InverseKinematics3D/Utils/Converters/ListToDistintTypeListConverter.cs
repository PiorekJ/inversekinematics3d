﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using InverseKinematics3D.Properties;

namespace InverseKinematics3D.Utils.Converters
{
    class ListToDistintTypeListConverter : IMultiValueConverter
    {

        //To use with multibinding, first value is the number of elements, second the collection
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2)
                return null;

            var vals = values[1] as IEnumerable;
            if (vals == null)
                return null;

            var types = new List<string>();
            types.Add(Resources.ComboBoxFilterDefault);
            foreach (object item in vals)
            {
                types.Add(item.GetType().FullName);
            }

            return types.Distinct();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
