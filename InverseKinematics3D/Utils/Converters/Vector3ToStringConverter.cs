﻿using System;
using System.Globalization;
using System.Windows.Data;
using OpenTK;

namespace InverseKinematics3D.Utils.Converters
{
    class Vector3ToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String retValue = String.Empty;
            if (value != null)
            {
                Vector3 conv = (Vector3)value;
                retValue =
                    $"X: {conv.X.ToString("F3", CultureInfo.InvariantCulture)} Y: {conv.Y.ToString("F3", CultureInfo.InvariantCulture)} Z: {conv.Z.ToString("F3", CultureInfo.InvariantCulture)}";
            }

            return retValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
