﻿using System;
using System.Windows.Media;
using OpenTK;

namespace InverseKinematics3D.Utils
{
    public static class Extensions
    {
        public static Vector3 ColorToVector3(this Color color)
        {
            return new Vector3(color.ScR, color.ScG, color.ScB);
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        public static Vector3 EulerAngles(this Quaternion q)
        {
            double heading, attitude, bank;
            double test = q.X * q.Y + q.Z * q.W;
            Vector3 euler = Vector3.Zero;

            if (test > 0.499)
            { // singularity at north pole
                heading = 2 * System.Math.Atan2(q.X, q.W);
                attitude = System.Math.PI / 2;
                bank = 0;
                euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
                return euler;
            }
            if (test < -0.499)
            { // singularity at south pole
                heading = -2 * System.Math.Atan2(q.X, q.W);
                attitude = -System.Math.PI / 2;
                bank = 0;
                euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
                return euler;
            }
            double sqx = q.X * q.X;
            double sqy = q.Y * q.Y;
            double sqz = q.Z * q.Z;
            heading = System.Math.Atan2(2 * q.Y * q.W - 2 * q.X * q.Z, 1 - 2 * sqy - 2 * sqz);
            attitude = System.Math.Asin(2 * test);
            bank = System.Math.Atan2(2 * q.X * q.W - 2 * q.Y * q.Z, 1 - 2 * sqx - 2 * sqz);
            euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
            return euler;
        }

        public static Quaternion Negate(this Quaternion q)
        {
            return new Quaternion(-q.X, -q.Y, -q.Z, -q.W);
        }
    }
}
