﻿using System;
using System.Security.Policy;
using OpenTK;

namespace InverseKinematics3D.Utils.Math
{
    public static class MathGeneralUtils
    {
        public static Matrix4 CreatePerspectiveFOVMatrix(float fov, float aspectRatio, float zNear, float zFar)
        {

            if (fov <= 0 || fov > System.Math.PI)
                throw new ArgumentOutOfRangeException("Incorrect fov");
            if (aspectRatio <= 0)
                throw new ArgumentOutOfRangeException("Incorrect aspect ratio");
            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("Incorrect zNear");

            float yMax = zNear * (float)System.Math.Tan(0.5f * fov);
            float yMin = -yMax;
            float xMin = yMin * aspectRatio;
            float xMax = yMax * aspectRatio;

            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("zNear");

            float x = (2.0f * zNear) / (xMax - xMin);
            float y = (2.0f * zNear) / (yMax - yMin);
            float a = (xMax + xMin) / (xMax - xMin);
            float b = (yMax + yMin) / (yMax - yMin);
            float c = -(zFar + zNear) / (zFar - zNear);
            float d = -(2.0f * zFar * zNear) / (zFar - zNear);

            return new Matrix4(x, 0, 0, 0,
                0, y, 0, 0,
                a, b, c, -1,
                0, 0, d, 0);
        }

        public static Matrix4 CreateTranslation(Vector3 position)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row3 = new Vector4(position.X, position.Y, position.Z, 1.0f);
            return result;
        }

        public static Matrix4 CreateFromQuaternion(Quaternion rotation)
        {
            rotation.ToAxisAngle(out var axis, out var angle);

            float cos = (float)System.Math.Cos(-angle);
            float sin = (float)System.Math.Sin(-angle);
            float t = 1.0f - cos;

            axis.Normalize();

            Matrix4 result = Matrix4.Identity;
            result.Row0 = new Vector4(t * axis.X * axis.X + cos, t * axis.X * axis.Y - sin * axis.Z, t * axis.X * axis.Z + sin * axis.Y, 0.0f);
            result.Row1 = new Vector4(t * axis.X * axis.Y + sin * axis.Z, t * axis.Y * axis.Y + cos, t * axis.Y * axis.Z - sin * axis.X, 0.0f);
            result.Row2 = new Vector4(t * axis.X * axis.Z - sin * axis.Y, t * axis.Y * axis.Z + sin * axis.X, t * axis.Z * axis.Z + cos, 0.0f);
            result.Row3 = Vector4.UnitW;
            return result;
        }

        public static Matrix4 CreateScale(Vector3 scale)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row0.X = scale.X;
            result.Row1.Y = scale.Y;
            result.Row2.Z = scale.Z;
            return result;
        }

        public static float Lerp(float from, float to, float t)
        {
            return (1 - t) * from + t * to;
        }

        public static Vector3 Lerp(Vector3 from, Vector3 to, float t)
        {
            return (1 - t) * from + t * to;
        }

        private static float Angle(Vector3 from, Vector3 to)
        {
            float denominator = (float)System.Math.Sqrt(from.LengthSquared * to.LengthSquared);
            if (denominator < Constants.Epsilon)
                return 0F;

            float dot = Clamp(Vector3.Dot(from, to) / denominator, -1.0f, 1.0f);
            return (float)MathHelper.RadiansToDegrees(System.Math.Acos(dot));
        }

        public static float SignedAngle(Vector3 from, Vector3 to, Vector3 axis)
        {
            float unsignedAngle = Angle(from, to);
            float sign = System.Math.Sign(Vector3.Dot(axis, Vector3.Cross(from, to)));
            return unsignedAngle * sign;
        }

        public static float CalculateAngle2(Vector3 v1, Vector3 v2)
        {
            var mag = (v1.Length * v2.Length);
            var dot = Vector3.Dot(v1, v2);
            var cross = Vector3.Cross(v1, v2);
            v1 = v1.Normalized();
            v2 = v2.Normalized();
            var mat = new Matrix3(v1.X, v2.X, 1, v1.Y, v2.Y, 1, v1.Z, v2.Z, 1);

            var sin = mat.Determinant;
            var cos = dot / mag;

            var acos = (float)System.Math.Acos(cos);
            var asin = (float)System.Math.Asin(sin);

            Console.WriteLine($"Acos: {MathHelper.RadiansToDegrees(acos)} Asin: {MathHelper.RadiansToDegrees(asin)}");

            if (sin >= 0 && cos >= 0)
            {
                Console.WriteLine("Octant 1");
            }

            if (sin >= 0 && cos < 0)
            {
                Console.WriteLine("Octant 2");
            }

            if (sin < 0 && cos < 0)
            {
                Console.WriteLine("Octant 3");
            }

            if (sin < 0 && cos >= 0)
            {
                Console.WriteLine("Octant 4");

            }

            return 0;
        }

        public static float AngleDistance(float a1, float a2)
        {
            return 180 - System.Math.Abs((System.Math.Abs(a1 - a2) % 360) - 180);
        }

        #region Euler

        //Change euler angles to quaternion
        public static Quaternion ToQuaternion(float z, float y, float x)
        {
            float cy = (float)System.Math.Cos(z * 0.5);
            float sy = (float)System.Math.Sin(z * 0.5);
            float cp = (float)System.Math.Cos(y * 0.5);
            float sp = (float)System.Math.Sin(y * 0.5);
            float cr = (float)System.Math.Cos(x * 0.5);
            float sr = (float)System.Math.Sin(x * 0.5);

            Quaternion q = Quaternion.Identity;
            q.W = cy * cp * cr + sy * sp * sr;
            q.X = cy * cp * sr - sy * sp * cr;
            q.Y = sy * cp * sr + cy * sp * cr;
            q.Z = sy * cp * cr - cy * sp * sr;
            return q;
        }

        // Create rotation matrix for X axis
        public static void CreateRotationX(float angle, out Matrix4 result)
        {
            var cos = (float)System.Math.Cos(angle);
            var sin = (float)System.Math.Sin(angle);

            result = Matrix4.Identity;
            result.Row1.Y = cos;
            result.Row1.Z = sin;
            result.Row2.Y = -sin;
            result.Row2.Z = cos;
        }

        // Create rotation matrix for Y axis
        public static void CreateRotationY(float angle, out Matrix4 result)
        {
            var cos = (float)System.Math.Cos(angle);
            var sin = (float)System.Math.Sin(angle);

            result = Matrix4.Identity;
            result.Row0.X = cos;
            result.Row0.Z = -sin;
            result.Row2.X = sin;
            result.Row2.Z = cos;
        }

        // Create rotation matrix for Z axis
        public static void CreateRotationZ(float angle, out Matrix4 result)
        {
            var cos = (float)System.Math.Cos(angle);
            var sin = (float)System.Math.Sin(angle);

            result = Matrix4.Identity;
            result.Row0.X = cos;
            result.Row0.Y = sin;
            result.Row1.X = -sin;
            result.Row1.Y = cos;
        }

        // Clamps value between min and max and returns value
        public static float Clamp(float value, float min, float max)
        {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        }

        // Loops the value t, so that it is never larger than length and never smaller than 0.
        public static float Repeat(float t, float length)
        {
            return Clamp(t - (float)System.Math.Floor(t / length) * length, 0.0f, length);
        }

        // Clamps value between 0 and 1 and returns value
        public static float Clamp01(float value)
        {
            if (value < 0F)
                return 0F;
            if (value > 1F)
                return 1F;
            return value;
        }

        // Lerps angle takin into respect 0-360 barriers
        public static float LerpAngle(float from, float to, float t)
        {
            float delta = Repeat((to - from), 360);
            if (delta > 180)
                delta -= 360;
            return from + delta * Clamp01(t);
        }

        #endregion

        #region Quaternion

        //Change quaternion to euler angles
        public static Vector3 ToEulerAngles(Quaternion q)
        {
            float x, y, z;
            q.Normalize();
            float sinr_cosp = 2.0f * (q.W * q.X + q.Y * q.Z);
            float cosr_cosp = 1.0f - 2.0f * (q.X * q.X + q.Y * q.Y);
            x = (float)System.Math.Atan2(sinr_cosp, cosr_cosp);

            float sinp = 2.0f * (q.W * q.Y - q.Z * q.X);
            if (System.Math.Abs(sinp) >= 1)
            {
                y = (float)System.Math.Sign(sinp) * MathHelper.PiOver2;
            }
            else
                y = (float)System.Math.Asin(sinp);

            float siny_cosp = 2.0f * (q.W * q.Z + q.X * q.Y);
            float cosy_cosp = 1.0f - 2.0f * (q.Y * q.Y + q.Z * q.Z);

            z = (float)System.Math.Atan2(siny_cosp, cosy_cosp);

            return new Vector3(MathHelper.RadiansToDegrees(z), MathHelper.RadiansToDegrees(y), MathHelper.RadiansToDegrees(x));
        }

        public static Quaternion Lerp(Quaternion from, Quaternion to, float t)
        {
            from.Normalize();
            to.Normalize();
            var dot = from.X * to.X + from.Y * to.Y + from.Z * to.Z + from.W * to.W;
            if (dot < 0.0f)
            {
                to = to.Negate();
            }
            return ((1 - t) * from + t * to).Normalized();
        }

        public static Quaternion SLerp(Quaternion from, Quaternion to, float t)
        {
            from.Normalize();
            to.Normalize();
            var dot = from.X * to.X + from.Y * to.Y + from.Z * to.Z + from.W * to.W;
            if (dot < 0.0f)
            {
                to = to.Negate();
                dot = -dot;
            }
            
            if (dot > Constants.QuaternionThreshold)
            {
                return Lerp(from, to, t);
            }

            float alpha = (float)System.Math.Acos(dot);
            float sinAlpha = (float)System.Math.Sin(alpha);
            var left = from * (float)(System.Math.Sin((1 - t) * alpha) / sinAlpha);
            var right = to * (float)(System.Math.Sin(t * alpha) / sinAlpha);
            return left + right;
        }

        #endregion


    }
}
