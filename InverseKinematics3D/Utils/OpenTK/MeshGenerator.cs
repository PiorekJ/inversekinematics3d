﻿using System;
using System.Collections.Generic;
using InverseKinematics3D.OpenTK;
using OpenTK;
using Color = System.Windows.Media.Color;

namespace InverseKinematics3D.Utils.OpenTK
{
    public static class MeshGenerator
    {

        public static void GenerateCylinderVertEdge(float radius, float verticalOffset, float length, int divLen, int divAngle, Vector3 color, Matrix3 rotation, Matrix4 translation, ref List<VertexPNC> vertices, ref List<uint> edges)
        {
            Func<double, double, Vector3> getPoint = (y, fi) =>
                new Vector3((float)System.Math.Cos(fi) * radius, (float)y, (float)System.Math.Sin(fi) * radius);
            Func<int, int, Vector3> getPointInd =
                (i, j) => getPoint(length / divLen * i + verticalOffset, 2 * System.Math.PI / divAngle * j);
            Func<int, int, Vector3> getNormalInd = (i, j) => getPoint(0, 2 * System.Math.PI / divAngle * j);

            //top
            uint middleVert = (uint)vertices.Count;
            vertices.Add(new VertexPNC(Vector3.TransformPosition(new Vector3(0, length + verticalOffset, 0) * rotation, translation), Vector3.UnitY * rotation, color));

            for (int i = 0; i < divAngle; i++)
            {
                uint currentVertIndex = (uint)vertices.Count;
                vertices.Add(new VertexPNC(Vector3.TransformPosition(getPoint(length + verticalOffset, 2 * System.Math.PI / divAngle * i) * rotation, translation), Vector3.UnitY * rotation, color));
                vertices.Add(
                    new VertexPNC(Vector3.TransformPosition(getPoint(length + verticalOffset, 2 * System.Math.PI / divAngle * (i + 1) % divAngle) * rotation, translation),
                        Vector3.UnitY * rotation, color));
                edges.Add(middleVert);
                edges.Add(currentVertIndex);
                edges.Add(currentVertIndex + 1);
            }

            //bottom
            middleVert = (uint)vertices.Count;
            vertices.Add(new VertexPNC(Vector3.TransformPosition(new Vector3(0, verticalOffset, 0) * rotation, translation), -Vector3.UnitY * rotation, color));

            for (int i = 0; i < divAngle; i++)
            {
                uint currentVertIndex = (uint)vertices.Count;
                vertices.Add(new VertexPNC(Vector3.TransformPosition(getPoint(verticalOffset, 2 * System.Math.PI / divAngle * i) * rotation, translation), -Vector3.UnitY * rotation, color));
                vertices.Add(
                    new VertexPNC(Vector3.TransformPosition(getPoint(verticalOffset, 2 * System.Math.PI / divAngle * (i + 1) % divAngle) * rotation,translation), -Vector3.UnitY * rotation, color));
                edges.Add(currentVertIndex + 1);
                edges.Add(currentVertIndex);
                edges.Add(middleVert);
            }

            //side
            for (int i = 0; i < divLen; i++)
            {
                for (int j = 0; j < divAngle; j++)
                {
                    uint currentVertIndex = (uint)vertices.Count;
                    vertices.Add(new VertexPNC(Vector3.TransformPosition(getPointInd(i, j) * rotation, translation), getNormalInd(i, j).Normalized() * rotation, color));
                    vertices.Add(new VertexPNC(Vector3.TransformPosition(getPointInd(i, (j + 1) % divAngle) * rotation, translation),
                        getNormalInd(i, (j + 1) % divAngle).Normalized() * rotation, color));
                    vertices.Add(new VertexPNC(Vector3.TransformPosition(getPointInd(i + 1, (j + 1) % divAngle) * rotation, translation),
                        getNormalInd(i + 1, (j + 1) % divAngle).Normalized() * rotation, color));
                    vertices.Add(new VertexPNC(Vector3.TransformPosition(getPointInd(i + 1, j) * rotation, translation), getNormalInd(i + 1, j).Normalized() * rotation, color));

                    edges.Add(currentVertIndex);
                    edges.Add(currentVertIndex + 1);
                    edges.Add(currentVertIndex + 3);
                    edges.Add(currentVertIndex + 1);
                    edges.Add(currentVertIndex + 2);
                    edges.Add(currentVertIndex + 3);
                }
            }
        }

        public static Mesh<VertexPNC> GenerateCylinderMesh(float radius = 0.5f, float verticalOffset = 0f, float length = 3f, int divLen = 5, int divAngle = 32, Vector3 color = default(Vector3), Matrix3 rotation = default(Matrix3), Matrix4 translation = default(Matrix4))
        {
            List<VertexPNC> vertices = new List<VertexPNC>();
            List<uint> edges = new List<uint>();
            GenerateCylinderVertEdge(radius, verticalOffset, length, divLen, divAngle, color, rotation, translation, ref vertices, ref edges);
            return new Mesh<VertexPNC>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        public static void GenerateSphereVertEdgeWireframe(float radius, int rings, int sectors, bool isHalf, float verticalOffset, int startIdx, ref List<VertexPN> vertices, ref List<uint> edges)
        {
            float R = 1.0f / (float)(rings - 1);
            float S = 1.0f / (float)(sectors - 1);

            if (isHalf)
            {
                rings /= 2;
                rings += 1;
            }

            for (int r = 0; r < rings; ++r)
            {
                for (int s = 0; s < sectors; ++s)
                {
                    float y = (float)System.Math.Sin(-MathHelper.PiOver2 + MathHelper.Pi * r * R);
                    float x = (float)System.Math.Cos(2 * MathHelper.Pi * s * S) * (float)System.Math.Sin(MathHelper.Pi * r * R);
                    float z = (float)System.Math.Sin(2 * MathHelper.Pi * s * S) * (float)System.Math.Sin(MathHelper.Pi * r * R);

                    //TexCoords.Add(new Vector2(s * S, r * R));
                    vertices.Add(new VertexPN(new Vector3(x * radius, y * radius + verticalOffset, z * radius), new Vector3(x, y, z)));
                    if (r < rings - 1)
                    {
                        int curRow = r * sectors;
                        int nextRow = (r + 1) * sectors;
                        int nextS = (s + 1) % sectors;

                        edges.Add((uint)(startIdx + curRow + s));
                        edges.Add((uint)(startIdx + nextRow + s));

                        edges.Add((uint)(startIdx + curRow + s));
                        edges.Add((uint)(startIdx + curRow + nextS));
                    }
                }
            }
        }

        public static void GenerateCubeMeshVert(float sizeX, float sizeY, float sizeZ, float offsetX, float offsetY, float offsetZ, Vector3 color, ref List<VertexPNC> vertices, ref List<uint> edges)
        {
            var sizeX2 = sizeX / 2.0f;
            var sizeY2 = sizeY / 2.0f;
            var sizeZ2 = sizeZ / 2.0f;

            Vector3 BLB = new Vector3(offsetX + -sizeX2, offsetY + -sizeY2, offsetZ + sizeZ2);
            Vector3 BLF = new Vector3(offsetX + -sizeX2, offsetY + -sizeY2, offsetZ + -sizeZ2);
            Vector3 BRB = new Vector3(offsetX + sizeX2, offsetY +  -sizeY2, offsetZ + sizeZ2);
            Vector3 BRF = new Vector3(offsetX + sizeX2, offsetY + -sizeY2, offsetZ + -sizeZ2);

            Vector3 TLB = new Vector3(offsetX + -sizeX2, offsetY + sizeY2, offsetZ + sizeZ2);
            Vector3 TLF = new Vector3(offsetX + -sizeX2, offsetY + sizeY2, offsetZ + -sizeZ2);
            Vector3 TRB = new Vector3(offsetX + sizeX2, offsetY + sizeY2, offsetZ + sizeZ2);
            Vector3 TRF = new Vector3(offsetX + sizeX2, offsetY + sizeY2, offsetZ + -sizeZ2);

            Vector3 TopN = -Vector3.UnitY;
            Vector3 BottomN = Vector3.UnitY;
            Vector3 FrontN = -Vector3.UnitZ;
            Vector3 BackN = Vector3.UnitZ;
            Vector3 RightN = -Vector3.UnitX;
            Vector3 LeftN = Vector3.UnitX;

            uint edgeOffset = (uint)vertices.Count;

            //Front face 0 1 2 3 
            vertices.Add(new VertexPNC(BLB, BackN, color));
            vertices.Add(new VertexPNC(BRB, BackN, color));
            vertices.Add(new VertexPNC(TRB, BackN, color));
            vertices.Add(new VertexPNC(TLB, BackN, color));

            //Back face 4 5 6 7
            vertices.Add(new VertexPNC(BLF, FrontN, color));
            vertices.Add(new VertexPNC(BRF, FrontN, color));
            vertices.Add(new VertexPNC(TRF, FrontN, color));
            vertices.Add(new VertexPNC(TLF, FrontN, color));

            //Top face 8 9 10 11
            vertices.Add(new VertexPNC(TLB, BottomN, color));
            vertices.Add(new VertexPNC(TRB, BottomN, color));
            vertices.Add(new VertexPNC(TRF, BottomN, color));
            vertices.Add(new VertexPNC(TLF, BottomN, color));

            //Bottom face 12 13 14 15
            vertices.Add(new VertexPNC(BLB, TopN, color));
            vertices.Add(new VertexPNC(BRB, TopN, color));
            vertices.Add(new VertexPNC(BRF, TopN, color));
            vertices.Add(new VertexPNC(BLF, TopN, color));

            //Right face 16 17 18 19
            vertices.Add(new VertexPNC(BRB, LeftN, color));
            vertices.Add(new VertexPNC(BRF, LeftN, color));
            vertices.Add(new VertexPNC(TRF, LeftN, color));
            vertices.Add(new VertexPNC(TRB, LeftN, color));

            //Left face 20 21 22 23
            vertices.Add(new VertexPNC(BLB, RightN, color));
            vertices.Add(new VertexPNC(BLF, RightN, color));
            vertices.Add(new VertexPNC(TLF, RightN, color));
            vertices.Add(new VertexPNC(TLB, RightN, color));



            //Back
            edges.Add(edgeOffset + 1);
            edges.Add(edgeOffset + 2);
            edges.Add(edgeOffset + 0);
            edges.Add(edgeOffset + 2);
            edges.Add(edgeOffset + 3);
            edges.Add(edgeOffset + 0);
            //Front
            edges.Add(edgeOffset + 6);
            edges.Add(edgeOffset + 5);
            edges.Add(edgeOffset + 4);
            edges.Add(edgeOffset + 7);
            edges.Add(edgeOffset + 6);
            edges.Add(edgeOffset + 4);

            //Top
            edges.Add(edgeOffset + 9);
            edges.Add(edgeOffset + 10);
            edges.Add(edgeOffset + 8);
            edges.Add(edgeOffset + 10);
            edges.Add(edgeOffset + 11);
            edges.Add(edgeOffset + 8);
            //Bottom
            edges.Add(edgeOffset + 14);
            edges.Add(edgeOffset + 13);
            edges.Add(edgeOffset + 12);
            edges.Add(edgeOffset + 15);
            edges.Add(edgeOffset + 14);
            edges.Add(edgeOffset + 12);
            //Right
            edges.Add(edgeOffset + 17);
            edges.Add(edgeOffset + 18);
            edges.Add(edgeOffset + 16);
            edges.Add(edgeOffset + 18);
            edges.Add(edgeOffset + 19);
            edges.Add(edgeOffset + 16);
            //Left
            edges.Add(edgeOffset + 22);
            edges.Add(edgeOffset + 21);
            edges.Add(edgeOffset + 20);
            edges.Add(edgeOffset + 23);
            edges.Add(edgeOffset + 22);
            edges.Add(edgeOffset + 20);
        }

        //public static Mesh<VertexPNC> GenerateCubeMesh(float x, float y, float z)
        //{
        //    List<VertexPNC> vertices = new List<VertexPNC>();
        //    List<uint> edges = new List<uint>();
        //    GenerateCubeMeshVert(x, y, z, 0,0,0, Vector3.UnitX, ref vertices, ref edges);
        //    return new Mesh<VertexPNC>(vertices, edges, MeshType.Triangles, AccessType.Static);
        //}

        public static Mesh<VertexPN> GenerateSphereMeshWireframe(float radius = 0.5f, float verticalOffset = 0, int rings = 32, int sectors = 32, bool isHalf = false)
        {
            List<VertexPN> vertices = new List<VertexPN>();
            List<uint> edges = new List<uint>();
            GenerateSphereVertEdgeWireframe(radius, rings, sectors, isHalf, verticalOffset, 0, ref vertices, ref edges);
            return new Mesh<VertexPN>(vertices, edges, MeshType.Lines, AccessType.Static);
        }

        public static Mesh<VertexPNC> GenerateCursorCubeMesh(float axisWidth, float axisLength, float cubeOnEndSize, Color color)
        {
            List<VertexPNC> vertices = new List<VertexPNC>();
            List<uint> edges = new List<uint>();
            GenerateCubeMeshVert(axisLength, axisWidth, axisWidth, axisLength / 2.0f, 0,0, Vector3.UnitX, ref vertices, ref edges);
            GenerateCubeMeshVert(axisWidth, axisLength, axisWidth, 0, axisLength/2.0f, 0, Vector3.UnitY, ref vertices, ref edges);
            GenerateCubeMeshVert(axisWidth, axisWidth, axisLength, 0,0, axisLength/2.0f, Vector3.UnitZ, ref vertices, ref edges);
            GenerateCubeMeshVert(cubeOnEndSize, cubeOnEndSize, cubeOnEndSize, 0, 0, 0, color.ColorToVector3(), ref vertices, ref edges);
            return new Mesh<VertexPNC>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }
    }
}
