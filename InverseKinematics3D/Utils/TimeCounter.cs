﻿using System.Diagnostics;

namespace InverseKinematics3D.Utils
{
    public class TimeCounter
    {
        private Stopwatch _stopwatch;
        public static float DeltaTime;
        private int frames;


        public TimeCounter()
        {
            _stopwatch = new Stopwatch();
            DeltaTime = 0;
        }

        public float CountDT()
        {
            _stopwatch.Stop();
            DeltaTime = (float)_stopwatch.Elapsed.TotalSeconds;
            _stopwatch.Reset();
            _stopwatch.Start();
            return DeltaTime;
        }

        private float timeFrame = 0.0f;
        int currFrames = 0;
        public int CountFPS(float dt)
        {
            timeFrame += dt;
            ++frames;
            currFrames = 0;
            if (timeFrame > 1.0f)
            {
                currFrames = (int)(frames / timeFrame);
                timeFrame -= 1.0f;
                frames = 0;
                return currFrames;
            }
            return 0;
        }

        public void Start()
        {
            _stopwatch.Start();
        }
    }
}
